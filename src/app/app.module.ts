import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponentComponent } from './main-component/main-component.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { NetworkComponent } from './network/network.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { ThreadsComponent } from './threads/threads.component';
//import { RouterModule }   from '@angular/router';
//
//RouterModule.forRoot([
//  {
//    path: 'login',
//    component: LoginComponent
//
//  }
//])

const appRoutes: Routes = [
  { path: 'logout', component: LoginComponent },
  { path: '', component: MainComponentComponent },
  { path: 'network', component: NetworkComponent },
  { path: 'threads', component: ThreadsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponentComponent,
    NavbarComponent,
    LoginComponent,
    NetworkComponent,
    UserDetailsComponent,
    ThreadsComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { useHash: true } // <-- debugging purposes only
    ),
    // other imports here

    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
