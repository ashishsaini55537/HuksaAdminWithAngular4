import { HuksaadminAng4Page } from './app.po';

describe('huksaadmin-ang4 App', () => {
  let page: HuksaadminAng4Page;

  beforeEach(() => {
    page = new HuksaadminAng4Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
